package com.example.tp3;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
//import com.example.tp3.webservice.WebServiceGet;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;


    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;
    private SportDbHelper db ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);
        db = new SportDbHelper(this);
        team = (Team) getIntent().getParcelableExtra(Team.TAG);
        final SportDbHelper db = new SportDbHelper(this);
        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        updateView();
        Log.d("naaame", team.getName());
        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO

                new FetchTask().execute();
                updateView();
            }
        });

    }

    /*@Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        super.onBackPressed();
    }*/

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

	//TODO : update imageBadge
    }

    private class FetchTask extends AsyncTask<Integer, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Integer... integers) {
            Log.d("naaames", team.getName());
            //WebServiceUrl site=new WebServiceUrl();
            JSONResponseHandlerTeam teaminfo = new JSONResponseHandlerTeam(team);
            JSONResponseHandlerClassement teaminfo2 = new JSONResponseHandlerClassement(team);
            JSONResponseHandlerMatch teaminfo3 = new JSONResponseHandlerMatch(team);
            try {
                URL url = WebServiceUrl.buildSearchTeam(team.getName());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                InputStream in = new BufferedInputStream(connection.getInputStream());
                teaminfo.readJsonStream(in);

                long id = team.getId();
                team=teaminfo.getTeam();
                team.setId(id);
                db.updateTeam(team);
                Log.d("naaames", team.getStadium());

                url = WebServiceUrl.buildGetRanking(team.getIdLeague());
                connection = (HttpURLConnection) url.openConnection();
                in = new BufferedInputStream(connection.getInputStream());
                teaminfo2.readJsonStream(in);

                team=teaminfo.getTeam();
                team.setId(id);
                db.updateTeam(team);


                url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                connection = (HttpURLConnection) url.openConnection();
                in = new BufferedInputStream(connection.getInputStream());
                teaminfo3.readJsonStream(in);

                team=teaminfo.getTeam();
                team.setId(id);
                db.updateTeam(team);

            } catch (MalformedURLException e) {
                e.printStackTrace();
                Log.d("aaaames", team.getName());
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("caaames", team.getName());
            }

            return true;
        }


        @Override
        protected void onPostExecute(Boolean s) {
            super.onPostExecute(s);

           updateView();
        }
    }
}
