package com.example.tp3;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerMatch {
    private static final String TAG = JSONResponseHandlerClassement.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerMatch(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }


    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("results")) {
                readArrayTeams(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayTeams(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        int temp;
        Match m = new Match();
        while (reader.hasNext()) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb == 0) {
                    if (name.equals("strEvent")) {
                        //team.setTotalPoints(reader.nextInt());
                        m.setLabel(reader.nextString());
                    }
                    else if(name.equals("strHomeTeam"))
                    {
                        m.setHomeTeam(reader.nextString());
                    }
                    else if(name.equals("strAwayTeam"))
                    {
                        m.setAwayTeam(reader.nextString());
                    }
                    else if(name.equals("intHomeScore")) {
                        JsonToken check = reader.peek();

                        if (check == JsonToken.NULL) {
                            reader.nextNull();
                            m.setHomeScore(-1);
                        }
                        else
                        {
                            m.setHomeScore(reader.nextInt());
                        }

                    }
                    else if(name.equals("intAwayScore"))
                    {

                        JsonToken check = reader.peek();

                        if (check == JsonToken.NULL) {
                            reader.nextNull();
                            m.setAwayScore(-1);
                        }
                        else
                        {
                            m.setAwayScore(reader.nextInt());
                        }
                    }
                    else {
                         reader.skipValue();
                    }
                    team.setLastEvent(m);
            }  else {
                reader.skipValue();
            }
        }
        reader.endObject();
        nb++;
    }
        reader.endArray();
}
    public Team getTeam() {
        return team;
    }
}
