package com.example.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static com.example.tp3.SportDbHelper.COLUMN_LEAGUE_NAME;
import static com.example.tp3.SportDbHelper.COLUMN_TEAM_NAME;

public class MainActivity extends AppCompatActivity {
    private Team team;
    private SportDbHelper db ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        db = new SportDbHelper(this);
        final SportDbHelper sport = new SportDbHelper(this);
        //sport.populate();
        final Cursor cursor = sport.fetchAllTeams();
        final ListView vue = (ListView) findViewById(R.id.listview);
        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.listview, cursor, new String[] {COLUMN_TEAM_NAME, COLUMN_LEAGUE_NAME },  new int[] { R.id.list_row_title,R.id.list_row_content });
        vue.setAdapter(adapter);

        SwipeRefreshLayout mySwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.container);
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                      /*List<Team> teamlist= sport.getAllTeams();
                      for(int i=0;i<teamlist.size();i++)
                      {
                          team = teamlist.get(i);
                          Log.d("baaaaaaaaaaaaaaaa",team.getName() );*/
                          new FetchTask().execute();
                      //}
                    }
                }
        );







        vue.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                Cursor cursor= sport.fetchAllTeams();
                cursor.moveToFirst();
                Team temp = null;
                while (cursor.moveToNext()) {
                    if (cursor.getPosition() == pos) {
                        temp = sport.cursorToTeam(cursor);
                    }
                }
                Intent TeamActivity = new Intent(getApplicationContext(), TeamActivity.class);
                TeamActivity.putExtra("Team",temp);
                adapter.notifyDataSetChanged();
                startActivity(TeamActivity);

            }

        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent NewTeamActivity = new Intent(getApplicationContext(), NewTeamActivity.class);
                startActivity(NewTeamActivity);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }








    private class FetchTask extends AsyncTask<Integer, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Integer... integers) {

            List<Team> teamlist= db.getAllTeams();
            for(int i=0;i<teamlist.size();i++)
            {
                Log.d("naaames", teamlist.get(i).getName());
                //WebServiceUrl site=new WebServiceUrl();
                JSONResponseHandlerTeam teaminfo = new JSONResponseHandlerTeam(teamlist.get(i));
                JSONResponseHandlerClassement teaminfo2 = new JSONResponseHandlerClassement(teamlist.get(i));
                JSONResponseHandlerMatch teaminfo3 = new JSONResponseHandlerMatch(teamlist.get(i));
                try {
                    URL url = WebServiceUrl.buildSearchTeam(teamlist.get(i).getName());
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    InputStream in = new BufferedInputStream(connection.getInputStream());
                    teaminfo.readJsonStream(in);

                    long id = teamlist.get(i).getId();
                    team=teaminfo.getTeam();
                    team.setId(id);
                    db.updateTeam(team);
                    Log.d("naaames", teamlist.get(i).getStadium());

                    url = WebServiceUrl.buildGetRanking(teamlist.get(i).getIdLeague());
                    connection = (HttpURLConnection) url.openConnection();
                    in = new BufferedInputStream(connection.getInputStream());
                    teaminfo2.readJsonStream(in);

                    team=teaminfo.getTeam();
                    team.setId(id);
                    db.updateTeam(team);


                    url = WebServiceUrl.buildSearchLastEvents(teamlist.get(i).getIdTeam());
                    connection = (HttpURLConnection) url.openConnection();
                    in = new BufferedInputStream(connection.getInputStream());
                    teaminfo3.readJsonStream(in);

                    team=teaminfo.getTeam();
                    team.setId(id);
                    db.updateTeam(team);

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    Log.d("aaaames", teamlist.get(i).getName());
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("caaames", teamlist.get(i).getName());
                }


            }
            return true;
        }



    }
}
